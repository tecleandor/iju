create table files
(
	filepath text not null
		constraint files_pk
			primary key,
	uid text,
	sent integer default 0 not null,
	sent_date text,
	file_error int default 0 not null,
	send_error int default 0 not null,
	commited int default 0 not null,
	commited_date text
);

create unique index files_filepath_uindex
	on files (filepath);

create table project
(
	key text not null
		constraint project_pk
			primary key,
	value text not null
);

create unique index project_key_uindex
	on project (key);
