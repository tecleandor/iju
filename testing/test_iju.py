import iju
import os
import tempfile
from pydicom.data import get_testdata_file

# TODO: Arrange for creating a new project?


def test_get_info_description():
    description = iju.get_info("testing/demodata/tape01.sqlite", "description")
    assert description == "Importing data tape number 01"


def test_get_info_non_existent():
    nonexistant = iju.get_info("testing/demodata/tape01.sqlite", "random1234")
    assert nonexistant is None


def test_is_dicom_valid():
    filename = get_testdata_file("rtplan.dcm")
    assert iju.is_dicom(filename) is True


def test_is_dicom_invalid():
    f = tempfile.NamedTemporaryFile(delete=False)
    f.write(b"IMNOTDICOM\n")
    isdicom = iju.is_dicom(f.name)
    f.close()
    os.unlink(f.name)
    assert iju.is_dicom(isdicom) is False
