"""
IJU is a python tool for analyzing and moving great quantities of DICOM studies
"""
import os
import sys
import sqlite3
import logging
import datetime
import time
from os.path import join
from collections import namedtuple
from math import ceil

import click
from pydicom import dcmread
from pynetdicom import AE
from pynetdicom.apps.storescu.storescu import get_contexts
from tqdm import tqdm


def dicom_echo(aetitle, host, port):
    """
    Function for doing a DICOM ECHO
    :param aetitle: Target AE
    :param host: Target host, IP or hostname
    :param port: Target DICOM port
    :return: named tuple with
    """
    ae = AE()
    ae.add_requested_context("1.2.840.10008.1.1")
    tic = time.perf_counter()
    assoc = ae.associate(host, int(port), ae_title=aetitle)
    Desc = namedtuple("Desc", ["result", "time"])
    if assoc.is_established:
        assoc.send_c_echo()
        assoc.release()
        print("Connected.")
        toc = time.perf_counter()
        return Desc("OK", toc - tic)

    if assoc.is_rejected:
        toc = time.perf_counter()
        return Desc("rejected", toc - tic)

    if assoc.is_aborted:
        toc = time.perf_counter()
        return Desc("aborted", toc - tic)

    toc = time.perf_counter()
    return Desc("unknown", toc - tic)


def create_connection(db_file):
    """create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    # TODO: Create database if not existing
    try:
        conn = sqlite3.connect(db_file)
    except sqlite3.Error as e:
        print(e)

    return conn


def get_info(project_file, parameter):
    """
    Gets the value for a config parameter stored on the sqlite database
    :param project_file: Path to the SQLite file
    :param parameter: Parameter to fetch
    :return: Parameter value
    """
    config_get = """ SELECT value FROM project WHERE key=? """
    if not os.path.isfile(project_file):
        sys.exit("Error: Project file doesn't exist")
    conn = create_connection(project_file)
    with conn:
        cur = conn.cursor()
        try:
            cur.execute(config_get, (parameter,))
        except sqlite3.DatabaseError:
            sys.exit("Error: file is not a proper SQLite database")
        results = cur.fetchone()
        if results is not None:
            return results[0]

        # TODO: Exception for no contents ?


def is_dicom(filepath):
    """
    Looks for a DICOM header in a file
    :param filepath:
    :return: is dicom?
    """
    with open(filepath, "rb") as f:
        f.seek(128, 0)
        byte = f.read(4)
        if byte.decode("utf-8") == "DICM":
            return True

        return False


def add_file(conn, filepath, uid):
    """
    Add a new file to the files table
    :param conn: sqlite connection
    :param filepath: relative full path for DICOM file
    :param uid: DICOM SOP UID
    :return: add status
    TODO: Status return ?
    """
    sql_insert = """ INSERT INTO files(filepath, uid)
              VALUES(?,?) """
    sql_search = """ SELECT * FROM files WHERE filepath=? """
    cur = conn.cursor()
    cur.execute(sql_search, (filepath,))
    results = cur.fetchone()
    if results is None:
        cur.execute(sql_insert, (filepath, uid))
        logging.debug("Added file to database: %s", filepath)
        conn.commit()
    else:
        logging.debug("Already existing file in database: %s", filepath)


def create_project(project_file, description):
    """
    Creates a new sqlite project file using schema.sql as a base
    :param project_file: path to sqlite database
    :param description: project description
    """
    config_insert = """ INSERT INTO project(key, value)
                  VALUES(?,?) """
    creation_date = (
        datetime.datetime.now().astimezone().replace(microsecond=0).isoformat()
    )
    conn = create_connection(project_file)
    with conn:
        with open("schema.sql") as fp:
            cur = conn.cursor()
            cur.executescript(fp.read())
        cur.execute(config_insert, ("description", description))
        cur.execute(config_insert, ("creationdate", creation_date))


@click.command("info")
@click.option(
    "--project-file",
    "-f",
    required=True,
    type=click.Path(exists=True, dir_okay=False, writable=True),
    help="path to the sqlite project file to review",
)
def info(project_file):
    """shows information about project file"""
    # TODO: Create data extraction function
    # TODO: Create exceptions
    count_files = """ SELECT COUNT(1) FROM files """

    print("Opening project file...")
    description = get_info(project_file, "description")
    creation_date = get_info(project_file, "creationdate")

    conn = create_connection(project_file)
    with conn:
        cur = conn.cursor()
        try:
            cur.execute(count_files)
        except sqlite3.DatabaseError:
            sys.exit("Error: file is not a proper SQLite database")
        results = cur.fetchone()
        number_of_files = results[0]
    print(f"Description: {description}")
    print(f"Creation date: {creation_date}")
    print(f"Files tracked: {number_of_files}")


@click.command("new")
@click.option(
    "--project-file",
    "-f",
    prompt=True,
    required=True,
    type=click.Path(dir_okay=False, exists=False, writable=True),
    help="path to the sqlite project file to create",
)
@click.option(
    "--project-description",
    "-d",
    prompt=True,
    required=True,
    help="description for this project",
)
def new(project_file, project_description):
    """Creates a new export project and generates its sqlite file"""
    print("Creating a new project...")
    if os.path.isfile(project_file):
        sys.exit("Error: Project file already exists")
    print("Creating sqlite database...")
    create_project(project_file, project_description)
    print(f"Done creating project in {project_file}")


@click.command("scan", no_args_is_help=True)
@click.option(
    "--scan-path",
    required=True,
    type=click.Path(file_okay=False, exists=True, readable=True),
    help="path to scan for DICOM files",
)
@click.option(
    "--project-file",
    required=True,
    type=click.Path(dir_okay=False, exists=True, writable=True),
    help="path to existing sqlite project file",
)
def scan(scan_path, project_file):
    """Scans recursively a given path into an existing database"""
    # TODO: Show better progress and stats
    logging.basicConfig(format="%(asctime)s %(message)s", level=logging.INFO)
    dicom_counter = 0
    not_dicom_counter = 0

    print("Connecting to database...")
    conn = create_connection(project_file)
    print(f"Scanning {scan_path}...")
    with conn:
        for root, _, files in os.walk(scan_path):
            for name in files:
                full_path = join(root, name)
                if is_dicom(full_path):
                    dataset = dcmread(full_path)
                    add_file(
                        conn, full_path, dataset.data_element("SOPInstanceUID").value
                    )
                    dicom_counter = dicom_counter + 1
                else:
                    not_dicom_counter = not_dicom_counter + 1
                if (dicom_counter + not_dicom_counter) % 100 == 0:
                    # Show one dot for every 100 files scanned
                    print(".", end="")
                    sys.stdout.flush()
        print("")
        print(f"{dicom_counter} dicom files")
        print(f"{not_dicom_counter} other files (not scanned)")


@click.command("echo", no_args_is_help=True)
@click.option("--aetitle", required=True, help="destination AE")
@click.option("--host", required=True, help="destination host")
@click.option(
    "--port",
    required=True,
    type=click.IntRange(min=1, max=65535),
    help="destination port",
)
def echo(aetitle, host, port):
    """ Connects to target to send a DICOM ECHO"""
    print(f"Connecting to {aetitle} at {host}:{port}")
    echo_result = dicom_echo(aetitle, host, port)
    if echo_result.result == "OK":
        print(f"Query time: {echo_result.time:0.3f} seconds")
    elif echo_result.result == "rejected":
        print(f"Query time: {echo_result.time:0.3f} seconds")
        sys.exit("ERROR: Association rejected")
    elif echo_result.result == "aborted":
        print(f"Query time: {echo_result.time:0.3f} seconds")
        sys.exit("ERROR: Association aborted")
    elif echo_result.result == "unknown":
        print(f"Query time: {echo_result.time:0.3f} seconds")
        sys.exit("ERROR: Unknown error")


@click.command("send", no_args_is_help=True)
@click.option("--aetitle", required=True, help="destination AE")
@click.option("--host", required=True, help="destination host")
@click.option(
    "--port",
    required=True,
    type=click.IntRange(min=1, max=65535),
    help="destination port",
)
@click.option(
    "--project-file",
    required=True,
    type=click.Path(exists=True, dir_okay=False, writable=True),
    help="path to existing sqlite project file",
)
def send(aetitle, host, port, project_file):
    """
    Sends DICOM files from project to target
    """
    # TODO: Add option for number of files
    number_of_files = 100
    sql_search = (
        """SELECT filepath FROM files WHERE sent IS 0 AND send_error IS 0 LIMIT ?"""
    )
    sql_update_ok = """ UPDATE files SET sent = 1, sent_date = ? WHERE filepath IS ? """
    sql_update_not_ok = (
        """ UPDATE files SET send_error = 1, sent_date = ? WHERE filepath IS ? """
    )
    count_pending_files = (
        """ SELECT COUNT(1) FROM files WHERE sent IS 0 AND send_error IS 0"""
    )

    # Show project info
    print("Opening project file...")
    description = get_info(project_file, "description")
    creation_date = get_info(project_file, "creationdate")
    print(f"Description: {description}")
    print(f"Creation date: {creation_date}")

    # DICOM ECHO the server
    echo_result = dicom_echo(aetitle, host, port)
    if echo_result.result != "OK":
        sys.exit(f"Host {host} connection failed ")
    print(f"DICOM echo to {host} in {echo_result.time:0.3f}s")

    # Open the database
    conn = create_connection(project_file)
    with conn:
        cur = conn.cursor()
        try:
            cur.execute(count_pending_files)
            pending_files = cur.fetchone()[0]
            # Calculate number of loops
            loops = ceil(pending_files / number_of_files)
            print(f"Total files to send: {pending_files}")
            print(
                f"{loops} blocks of {number_of_files} files each will be sent to {aetitle}"
            )
            for _ in tqdm(
                range(loops),
                position=0,
                ncols=100,
                leave=False,
                colour="green",
                desc="Total blocks",
                unit="block",
            ):
                # Get list of files
                cur.execute(sql_search, (number_of_files,))
                rows = cur.fetchall()
                files = [x[0] for x in rows]
                # Lets set up (or reset) the AE
                ae = AE(ae_title="IJUDICOM")
                # Scan the files transfer syntax and
                # only propose required presentation contexts
                files, contexts = get_contexts(files, logging)
                try:
                    for abstract, transfer in contexts.items():
                        for syntax in transfer:
                            ae.add_requested_context(abstract, syntax)
                except ValueError as e:
                    raise ValueError(
                        "More than 128 presentation contexts required,"
                        "please try again with fewer files"
                    ) from e

                # Associate
                assoc = ae.associate(host, port, ae_title=aetitle)
                if assoc.is_established:
                    # Send studies
                    for row in tqdm(
                        rows,
                        desc="Partial send",
                        ncols=100,
                        unit="file",
                        unit_scale=1,
                        leave=False,
                        colour="yellow",
                        position=1,
                    ):
                        ds = dcmread(row[0])
                        status = assoc.send_c_store(ds)
                        # Check the status of the storage request
                        if status:
                            # If the storage request succeeded this will be 0x0000
                            # There are other status codes:
                            # https://pynetdicom2.readthedocs.io/en/latest/statuses.html
                            # TODO: Check rest of status codes
                            timestamp = (
                                datetime.datetime.now()
                                .astimezone()
                                .replace(microsecond=0)
                                .isoformat()
                            )
                            if status.Status == 0:
                                cur.execute(sql_update_ok, (timestamp, row[0]))
                                conn.commit()
                            else:
                                cur.execute(sql_update_not_ok, (timestamp, row[0]))
                                conn.commit()
                        else:
                            sys.exit(
                                "Connection timed out, was aborted or received invalid response"
                            )
                    assoc.release()
                else:
                    print("Association rejected, aborted or never connected")
        except sqlite3.DatabaseError:
            sys.exit("Error: file is not a proper SQLite database")
            # TODO: Again: I don't like this verification I should make a function somewhere


@click.group()
def main():
    """
    Iju is a tool that will help you moving great quantities of DICOM files
    """


main.add_command(scan)
main.add_command(new)
main.add_command(echo)
main.add_command(info)
main.add_command(send)

if __name__ == "__main__":
    main()
