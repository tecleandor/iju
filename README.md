# Ijū

Ijū is a tool for migrating or importing big quantities of DICOM data.

[[_TOC_]]

The idea is:
* [x] Scanning through storage for DICOM files
* [x] Have command line parameters (Click?)
* [x] Storing its UUIDs to track them (sqlite?)
* [x] Sending them to a target PACS
  * [ ] Partial sending (Only certain number of instances)
  * [ ] Parallel sending (Celery, Python-RQ?)
  * [ ] Dicomweb STOW-RS (Better performance?)  
* [ ] Verifying its safe arrival with DICOM Storage Commitment
* [ ] Review corrupted files and failed transmissions (datasette?)
* [ ] Build it as a fat distributable binary (nuitka?)

## Characteristics
* Python 3.6+
* sqlite
* pydicom
* pynetdicom

## Using Ijū

Ijū stores data and metadata about import/export tasks in sqlite files. This allows us to stop and start the process at 
our will, do it in batches, keep information for later, produce reports, debug problems...

### Creating a new project

You can create a new project running:
```
python iju.py new
```

Iju will prompt you for the needed parameters:
```
$ python iju.py new
Project file: tape01.sqlite
Project description: Importing data tape number 01
Creating a new project...
Creating sqlite database...
Done creating project in tape01.sqlite
```

|Options||
|:---:|:---|
|Project file|It's the full path for the sqlite file where we'll store the project info|
|Project description|This description will be saved on the project file for future reference|

You can also pass the parameters on command line instead of interactively:
```
$ python iju.py new --project-file tape01.sqlite --project-description "Importing data tape number 01"
Creating a new project...
Creating sqlite database...
Done creating project in tape01.sqlite
```

Iju will complain if the project file already exists and won't overwrite it:
```
$ python iju.py new --project-file tape01.sqlite --project-description "Importing data tape number 01"
Creating a new project...
Error: Project file already exists
```

### Viewing project information

You can ask Iju for information on a project file (the SQLite database), and you'll get some data and metadata from it.

It's as easy as passing the file path to the `view` option like this:
```
$ python iju.py info -f tape01.sqlite                   
Opening project file...
Description: Importing data tape number 01
Creation date: 2021-02-23T12:17:28+01:00
Files tracked: 16
``` 

|Options||
|:---:|:---|
|Description|The project description as given on project creation|
|Creation date|Timestamp for the project creation|
|Files tracked|Count for DICOM files tracked on the database|

### Sending studies

Once you have scanned some path(s) for DICOM files, you can send those
files to a destination PACS or AE.

Ijū will associate with destination AE and send your files in blocks of 
100 for each association, to improve performance compared to associating 
once per each file.

Before associating Ijū will read the metadata for all the files in each 
block and request only the needed contexts.

So, to send those studies, this is your command:

```
python iju.py send --aetitle DEST_AETITLE --host DEST_HOSTNAME_OR_IP --port TCP_PORT --project-file YOURPROJECT.sqlite
```

Let's send some files to a PACS:

```
python iju.py send --aetitle ORTHANC --host 192.168.1.10 --port 11112 --project-file tape01.sqlite
Opening project file...
Description: Importing data tape number 01
Creation date: 2021-02-23T12:17:28+01:00
Connected.
DICOM echo to 127.0.0.1 in 0.025s
Total files to send: 16011
161 blocks of 100 files each will be sent to ORTHANC
Total blocks:   3%|█▍                                            | 5/161 [00:45<23:52,  9.18s/block]
Partial send:  64%|████████████████████████████▊                | 64.0/100 [00:04<00:02, 14.9file/s]
```

This shows you some info about the project and two progress bars:
- The first is the progress for the total send job (of 16011 files in this case)
- The second is for the current "block" of 100 files being sent

Every progress bar has a speed rate indicator, with "seconds per block" 
and "files per second". Also, there is an estimated time to finish.  

## Performance

32 seconds for scanning 10847 files (all DICOM), in an SMB mounted share.

---
---
An Ijū ( 異獣 ) is a yōkai that [helped a porter to move a heavy load](https://yokai.fandom.com/wiki/Ij%C5%AB).  
Also, ijū ( 移住 ) means "emigration".